package com.ivancortes.todoen1.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by christian.vargas on 3/25/18.
 */

public class AnnouncementsList {

    @SerializedName("results")
    private ArrayList<Announcement> announcements;

    public ArrayList<Announcement> getAnnouncements() {
        return announcements;
    }

    public void setAnnouncements(ArrayList<Announcement> announcements) {
        this.announcements = announcements;
    }
}
