package com.ivancortes.todoen1.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by christian.vargas on 3/26/18.
 */

public class Provider {

    private String id;
    private String name;
    @SerializedName("category_name")
    private String categoryName;
    private String address;
    @SerializedName("category_icon")
    private String image;
    private String phone;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
