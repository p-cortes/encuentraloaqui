package com.ivancortes.todoen1.models;

/**
 * Created by christian.vargas on 3/25/18.
 */

public class Announcement {

    private String id;
    private String name;
    private String image;
    private boolean isLoading;

    public Announcement(boolean isLoading){
        this.isLoading = isLoading;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }
}
