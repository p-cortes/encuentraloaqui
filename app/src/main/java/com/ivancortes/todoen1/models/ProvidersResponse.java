package com.ivancortes.todoen1.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by christian.vargas on 3/26/18.
 */

public class ProvidersResponse {

    @SerializedName("results")
    private ArrayList<Provider> providerArrayList;

    public ArrayList<Provider> getProviderArrayList() {
        return providerArrayList;
    }

    public void setProviderArrayList(ArrayList<Provider> providerArrayList) {
        this.providerArrayList = providerArrayList;
    }
}
