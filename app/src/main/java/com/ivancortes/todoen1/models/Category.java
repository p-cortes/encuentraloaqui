package com.ivancortes.todoen1.models;

/**
 * Created by ivancortes on 25/03/18.
 */

public class Category {

    private String id;
    private String name;
    private String description;
    private String icon;
    private boolean lastPosition;
    private boolean isLoading;

    public Category(String id, String name, String description, String icon, boolean lastPosition) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.icon = icon;
        this.lastPosition = lastPosition;
    }

    public Category(boolean isLoading) {
        this.isLoading = isLoading;
     }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public boolean isLastPosition() {
        return lastPosition;
    }

    public void setLastPosition(boolean lastPosition) {
        this.lastPosition = lastPosition;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }
}

