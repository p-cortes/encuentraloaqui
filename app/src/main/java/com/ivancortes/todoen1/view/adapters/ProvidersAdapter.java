package com.ivancortes.todoen1.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ivancortes.todoen1.R;
import com.ivancortes.todoen1.models.Provider;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by christian.vargas on 3/26/18.
 */

public class ProvidersAdapter extends RecyclerView.Adapter<ProvidersAdapter.ProvidersViewHolder> {

    private View.OnClickListener onClickListener;
    private Context context;
    private ArrayList<Provider> providerArrayList;

    public ProvidersAdapter(Context context, View.OnClickListener onClickListener, ArrayList<Provider> providerArrayList) {
        this.context = context;
        this.onClickListener = onClickListener;
        this.providerArrayList = providerArrayList;
    }

    @NonNull
    @Override
    public ProvidersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_provider, parent, false);
        return new ProvidersAdapter.ProvidersViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProvidersViewHolder holder, int position) {
        Provider provider = providerArrayList.get(position);
        holder.mName.setText(provider.getName());
        holder.mCategory.setText(provider.getCategoryName());
        holder.mAddress.setText(provider.getAddress());
        Glide.with(context)
                .load(provider.getImage())
                .into(holder.mImage);
    }

    @Override
    public int getItemCount() {
        return providerArrayList.size();
    }

    public void update(ArrayList<Provider> providerArrayList) {
        this.providerArrayList.clear();
        this.providerArrayList.addAll(providerArrayList);
        notifyDataSetChanged();
    }

    public class ProvidersViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_provider_name)
        TextView mName;
        @BindView(R.id.item_provider_category)
        TextView mCategory;
        @BindView(R.id.item_provider_image)
        ImageView mImage;
        @BindView(R.id.item_provider_call)
        ImageView mCall;
        @BindView(R.id.item_provider_address)
        TextView mAddress;


        public ProvidersViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
