package com.ivancortes.todoen1.view.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ivancortes.todoen1.R;
import com.ivancortes.todoen1.models.Category;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ivancortes on 25/03/18.
 */

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoriesViewHolder> {

    private View.OnClickListener onClickListener;
    private Context context;
    private ArrayList<Category> categoryArrayList;
    private boolean isList;

    public CategoriesAdapter(Context context, View.OnClickListener onClickListener, ArrayList<Category> categoryArrayList, boolean isList) {
        this.context = context;
        this.onClickListener = onClickListener;
        this.categoryArrayList = categoryArrayList;
        this.isList = isList;
    }

    @Override
    public CategoriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_category, parent, false);
        if (isList) {
            layoutView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_category_list, parent, false);
        }

        return new CategoriesAdapter.CategoriesViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(CategoriesViewHolder holder, int position) {
        Category category = categoryArrayList.get(position);

        if (category.isLoading()) {

            holder.lottieAnimationView.setVisibility(View.VISIBLE);
            holder.mImageView.setVisibility(View.GONE);

            holder.lottieAnimationView.setAnimation("loading.json");
            holder.lottieAnimationView.setRepeatMode(LottieDrawable.INFINITE);
            holder.lottieAnimationView.playAnimation();

        } else {
            holder.lottieAnimationView.setAnimation("loading.json");
            holder.lottieAnimationView.setRepeatMode(LottieDrawable.INFINITE);
            holder.lottieAnimationView.playAnimation();

            holder.mNameText.setText(category.getName());
            holder.mDescription.setText(category.getDescription());


            if (category.isLastPosition()) {
                holder.lottieAnimationView.cancelAnimation();
                holder.lottieAnimationView.setVisibility(View.GONE);
                holder.mImageView.setVisibility(View.VISIBLE);
                Glide.with(context)
                        .load(R.drawable.ic_right_arrow)
                        .into(holder.mImageView);
            } else {
                Glide.with(context)
                        .load(category.getIcon())
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {

                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                holder.lottieAnimationView.cancelAnimation();
                                holder.lottieAnimationView.setVisibility(View.GONE);
                                holder.mImageView.setVisibility(View.VISIBLE);
                                return false;
                            }
                        })
                        .into(holder.mImageView);

            }

            holder.cardView.setTag(category);
            holder.cardView.setOnClickListener(onClickListener);
        }

    }

    @Override
    public int getItemCount() {
        return categoryArrayList.size();
    }

    public void update(ArrayList<Category> categoryArrayList) {
        this.categoryArrayList.clear();
        this.categoryArrayList.addAll(categoryArrayList);
        notifyDataSetChanged();
    }

    public class CategoriesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_category_image)
        ImageView mImageView;
        @BindView(R.id.item_category_name)
        TextView mNameText;
        @BindView(R.id.item_category_description)
        TextView mDescription;
        @BindView(R.id.item_category)
        CardView cardView;
        @BindView(R.id.animation_view)
        LottieAnimationView lottieAnimationView;

        public CategoriesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
