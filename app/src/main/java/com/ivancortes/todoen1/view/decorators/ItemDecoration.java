package com.ivancortes.todoen1.view.decorators;

import android.graphics.Rect;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by christian.vargas on 12/12/17.
 */

public class ItemDecoration extends RecyclerView.ItemDecoration {

    private int offset = 0;

    public ItemDecoration(int offset) {
        this.offset = offset;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {


        GridLayoutManager.LayoutParams layoutParams = (GridLayoutManager.LayoutParams) view.getLayoutParams();

        if (layoutParams.getSpanIndex() % 2 == 0){
            outRect.top = offset;
            outRect.left = offset;
            outRect.right = offset / 2;
        }else{
            outRect.top = offset;
            outRect.right = offset;
            outRect.left = offset / 2;
        }

      /*  if ((parent.getChildLayoutPosition(view) % 2) == 0) {
            outRect.top = 20;
            outRect.left = 60;
            outRect.right = 20;
            outRect.bottom = 20;
        } else {
            outRect.top = 20;
            outRect.left = 20;
            outRect.right = -20;
            outRect.bottom = 20;
        }

        if (parent.getChildLayoutPosition(view) == 0 || parent.getChildLayoutPosition(view) == 1) {
            outRect.top = 20;
        }*/


    }
}
