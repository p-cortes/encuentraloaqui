package com.ivancortes.todoen1.view.activities;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.ivancortes.todoen1.R;
import com.ivancortes.todoen1.models.Category;
import com.ivancortes.todoen1.presenter.callback.CategoriesListCallback;
import com.ivancortes.todoen1.presenter.implementation.BasePresenter;
import com.ivancortes.todoen1.presenter.implementation.CategoriesListPresenter;
import com.ivancortes.todoen1.presenter.implementation.SearchPresenter;
import com.ivancortes.todoen1.view.adapters.CategoriesAdapter;
import com.ivancortes.todoen1.view.decorators.EqualSpacingItemDecoration;
import com.ivancortes.todoen1.view.decorators.ItemDecoration;
import com.ivancortes.todoen1.view.decorators.ItemOffsetDecoration;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoriesListActivity extends BaseActivity implements CategoriesListCallback, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.act_categories_recycler)
    RecyclerView recyclerView;
    @BindView(R.id.act_categories_swipe)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private CategoriesAdapter categoriesAdapter;
    private CategoriesListPresenter categoriesListPresenter;

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(true);
        setContentView(R.layout.activity_categories_list);
        ButterKnife.bind(this);
        updateView();
    }

    public void updateView(){
        toolbar.setTitle("Categorías");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(v -> finish());
        recyclerView.addItemDecoration(new ItemDecoration(32));
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(categoriesAdapter = new CategoriesAdapter(this, onClickListener, new ArrayList<Category>(), true));

        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.postDelayed(() -> {
            mSwipeRefreshLayout.setRefreshing(true);
            onRefresh();
        }, 800);
    }

    @Override
    protected BasePresenter getPresenter() {
        return categoriesListPresenter = new CategoriesListPresenter(this, this);
    }

    @Override
    public void onRefresh() {
        categoriesListPresenter.getCategories();
    }

    @Override
    public void onSuccessGetCategories(ArrayList<Category> category) {
        mSwipeRefreshLayout.setRefreshing(false);
        categoriesAdapter.update(category);
    }

    @Override
    public void onErrorGetCategories(String message) {
        mSwipeRefreshLayout.setRefreshing(false);

    }
}
