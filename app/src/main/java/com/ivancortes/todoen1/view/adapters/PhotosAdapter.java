package com.ivancortes.todoen1.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;
import com.bumptech.glide.Glide;
import com.ivancortes.todoen1.R;
import com.ivancortes.todoen1.models.Announcement;

import java.util.ArrayList;

/**
 * Created by christian.vargas on 3/25/18.
 */

public class PhotosAdapter extends PagerAdapter {

    private Context context;
    private ArrayList<Announcement> announcementArrayList;

    public PhotosAdapter(Context context, ArrayList<Announcement> announcementArrayList) {
        this.context = context;
        this.announcementArrayList = announcementArrayList;
    }

    @Override
    public int getCount() {
        return announcementArrayList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return (view == (LinearLayout) object);
    }

    public void update(ArrayList<Announcement> announcementArrayList) {
        this.announcementArrayList.clear();
        this.announcementArrayList.addAll(announcementArrayList);
        notifyDataSetChanged();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Announcement announcement = announcementArrayList.get(position);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.view_pager_announcement, container, false);
        ImageView img = (ImageView) v.findViewById(R.id.view_pager_image);

        Glide.with(context)
                .load(announcement.getImage())
                .into(img);
        container.addView(v);
        return v;
    }

    @Override
    public void destroyItem(View collection, int position, Object view) {
        ((ViewPager) collection).removeView((View) view);
    }
}
