package com.ivancortes.todoen1.view.activities;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.ivancortes.todoen1.R;
import com.ivancortes.todoen1.models.Provider;
import com.ivancortes.todoen1.models.ProvidersResponse;
import com.ivancortes.todoen1.presenter.callback.ProvidersListCallback;
import com.ivancortes.todoen1.presenter.implementation.BasePresenter;
import com.ivancortes.todoen1.presenter.implementation.ProvidersListPresenter;
import com.ivancortes.todoen1.utils.Constants;
import com.ivancortes.todoen1.view.adapters.ProvidersAdapter;
import com.ivancortes.todoen1.view.decorators.ItemDecoration;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProvidersListActivity extends BaseActivity implements ProvidersListCallback, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.act_provider_list_swipe)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.act_provider_list_recycler)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private String categoryId;
    ProvidersAdapter categoriesAdapter;
    private ProvidersListPresenter providersListPresenter;

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(true);
        setContentView(R.layout.activity_providers_list);

        ButterKnife.bind(this);
        categoryId = getIntent().getStringExtra(Constants.CATEGORY_ID);
        updateView();

    }

    private void updateView() {
        toolbar.setTitle("Servicios");
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(v -> finish());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(categoriesAdapter = new ProvidersAdapter(this, onClickListener, new ArrayList<Provider>()));

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.postDelayed(() -> {
            swipeRefreshLayout.setRefreshing(true);
            onRefresh();
        }, 800);
    }

    @Override
    public void onRefresh() {
        providersListPresenter.getProviders(categoryId);
    }

    @Override
    protected BasePresenter getPresenter() {
        return providersListPresenter = new ProvidersListPresenter(this, this);
    }

    @Override
    public void onSuccessGetProviders(ProvidersResponse providersResponse) {
        swipeRefreshLayout.setRefreshing(false);
        categoriesAdapter.update(providersResponse.getProviderArrayList());
    }

    @Override
    public void onErrorGetProviders(String message) {
        swipeRefreshLayout.setRefreshing(false);
    }
}
