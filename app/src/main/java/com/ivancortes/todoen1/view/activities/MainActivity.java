package com.ivancortes.todoen1.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;

import com.ivancortes.todoen1.R;
import com.ivancortes.todoen1.view.fragments.AllDayFragment;
import com.ivancortes.todoen1.view.fragments.ContactFragment;
import com.ivancortes.todoen1.view.fragments.RegistrationFragment;
import com.ivancortes.todoen1.view.fragments.SearchFragment;


public class MainActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = findViewById(R.id.id_navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        setFragment(new SearchFragment());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.m1) {
            Intent i = new Intent(this, SearchActivity.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = item -> {
        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.navigation_search:
                fragment = new SearchFragment();
                break;
            case R.id.navigation_registration:
                fragment = new RegistrationFragment();
                break;
            case R.id.navigation_24_hours:
                fragment = new AllDayFragment();
                break;
            case R.id.navigation_contact:
                fragment = new ContactFragment();
                break;
        }
        setFragment(fragment);
        return true;
    };

    public void setFragment(Fragment set_fragment) {
        try {
            if (set_fragment != null) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.act_main_container, set_fragment);
                fragmentTransaction.commitAllowingStateLoss();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

}
