package com.ivancortes.todoen1.view.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ivancortes.todoen1.R;
import com.ivancortes.todoen1.presenter.implementation.BasePresenter;

import butterknife.ButterKnife;

/**
 * Created by ivancortes on 25/03/18.
 */

public class ProfileFragment extends BaseFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }
}
