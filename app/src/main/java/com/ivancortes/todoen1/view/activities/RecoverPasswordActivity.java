package com.ivancortes.todoen1.view.activities;

import android.os.Bundle;

import com.ivancortes.todoen1.R;

import butterknife.ButterKnife;

/**
 * Created by ivancortes on 25/03/18.
 */

public class RecoverPasswordActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarEnabled(false);
        setContentView(R.layout.activity_recover_password);
        ButterKnife.bind(this);
    }
}
