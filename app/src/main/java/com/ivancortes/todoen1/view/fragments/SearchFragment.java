package com.ivancortes.todoen1.view.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieDrawable;
import com.ivancortes.todoen1.R;
import com.ivancortes.todoen1.background.ws.responses.CategoriesResponse;
import com.ivancortes.todoen1.models.AnnouncementsList;
import com.ivancortes.todoen1.models.Category;
import com.ivancortes.todoen1.presenter.callback.SearchCallback;
import com.ivancortes.todoen1.presenter.implementation.BasePresenter;
import com.ivancortes.todoen1.presenter.implementation.SearchPresenter;
import com.ivancortes.todoen1.utils.Constants;
import com.ivancortes.todoen1.utils.MessageUtils;
import com.ivancortes.todoen1.view.activities.CategoriesListActivity;
import com.ivancortes.todoen1.view.activities.ProvidersListActivity;
import com.ivancortes.todoen1.view.adapters.CategoriesAdapter;
import com.ivancortes.todoen1.view.adapters.PhotosAdapter;
import com.ivancortes.todoen1.view.decorators.ItemOffsetDecoration;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ivancortes on 16/03/18
 */

public class SearchFragment extends BaseFragment implements SearchCallback {


    @BindView(R.id.fr_search_category_recycler)
    RecyclerView recyclerView;
    CategoriesAdapter categoriesAdapter;
    @BindView(R.id.fr_search_category_pager)
    ViewPager viewPager;
    @BindView(R.id.fr_search_category_dots)
    TabLayout tabLayout;
    @BindView(R.id.animation_view)
    LottieAnimationView lottieAnimationView;

    private SearchPresenter searchPresenter;
    PagerAdapter pagerAdapter;
    private View.OnClickListener onClickListener = v -> {
        Category category = (Category) v.getTag();
        onClickCategory(category);
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        updateView();
    }

    private void updateView() {
        lottieAnimationView.setAnimation("progress_bar.json");
        lottieAnimationView.setRepeatMode(LottieDrawable.INFINITE);
        lottieAnimationView.playAnimation();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        recyclerView.setAdapter(categoriesAdapter = new CategoriesAdapter(getContext(), onClickListener, searchPresenter.generateHolderAdapter(), false));
        recyclerView.addItemDecoration(new ItemOffsetDecoration(18));
        searchPresenter.getAnnouncement();
        searchPresenter.getCategories();


    }

    private void onClickCategory(Category category) {
        if (category.isLastPosition()) {
            Intent intent = new Intent(getContext(), CategoriesListActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(getContext(), ProvidersListActivity.class);
            intent.putExtra(Constants.CATEGORY_ID, category.getId());
            startActivity(intent);
        }
    }


    @Override
    public void onSuccessGettingCategories(CategoriesResponse categoriesResponse) {
        categoriesAdapter.update(categoriesResponse.getCategory());
    }

    @Override
    public void onSuccessGetAnnouncement(AnnouncementsList announcementsList) {
        pagerAdapter = new PhotosAdapter(getContext(), announcementsList.getAnnouncements());
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager, true);
        lottieAnimationView.cancelAnimation();
        lottieAnimationView.setVisibility(View.GONE);
        viewPager.setVisibility(View.VISIBLE);
    }


    @Override
    public void onErrorGettingCategories(String message) {

    }


    @Override
    protected BasePresenter getPresenter() {
        return searchPresenter = new SearchPresenter(getContext(), this);
    }
}
