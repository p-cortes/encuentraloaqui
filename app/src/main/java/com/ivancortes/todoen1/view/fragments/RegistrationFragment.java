package com.ivancortes.todoen1.view.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.TwitterAuthProvider;
import com.ivancortes.todoen1.R;
import com.ivancortes.todoen1.view.activities.RecoverPasswordActivity;
import com.ivancortes.todoen1.view.activities.RegisterActivity;
import com.ivancortes.todoen1.view.activities.RegisterUserActivity;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Executor;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

/**
 * Created by ivancortes on 16/03/18.
 */

public class RegistrationFragment extends Fragment implements FacebookCallback<LoginResult>, GraphRequest.GraphJSONObjectCallback {

    @BindView(R.id.frag_registration_login_facebook)
    LoginButton mLoginFBButton;
    @BindView(R.id.frag_registration_login_twitter)
    TwitterLoginButton mLoginTWButton;
    @BindView(R.id.act_login_pass)
    EditText actLoginPass;
    private CallbackManager callbackManager;
    private FirebaseAuth mAuth;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        View v = inflater.inflate(R.layout.fragment_registration, container, false);
        FirebaseApp.initializeApp(getContext());
        ButterKnife.bind(this, v);
        configureLoginFacebook();
        configureLoginTwitter();
        return v;
    }

    @OnClick(R.id.frag_btn_register)
    public void onClickRegister() {
        startActivity(new Intent(getActivity(), RegisterUserActivity.class));
    }

    @OnClick(R.id.frag_registration_recover_pass)
    public void onClickRecoverPass() {
        startActivity(new Intent(getActivity(), RecoverPasswordActivity.class));
    }

    @OnTouch(R.id.act_login_visible_pass)
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                actLoginPass.setInputType(InputType.TYPE_CLASS_TEXT);
                break;
            case MotionEvent.ACTION_UP:
                actLoginPass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                break;
        }
        return true;
    }


    public void configureLoginFacebook() {
        callbackManager = CallbackManager.Factory.create();
        mLoginFBButton.setReadPermissions("email", "user_birthday");
        mLoginFBButton.registerCallback(callbackManager, this);
    }

    public void configureLoginTwitter() {
        mAuth = FirebaseAuth.getInstance();
        mLoginTWButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                handleTwitterSession(result.data);
            }

            @Override
            public void failure(TwitterException exception) {

            }
        });
    }

    private void handleTwitterSession(TwitterSession session) {
        Log.d("TwitterSession", "handleTwitterSession:" + session);
        AuthCredential credential = TwitterAuthProvider.getCredential(
                session.getAuthToken().token,
                session.getAuthToken().secret);

        mAuth.signInWithCredential(credential).addOnCompleteListener((Executor) this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    FirebaseUser user = mAuth.getCurrentUser();
                    Log.d("Email", user.getEmail());
                    Log.d("Photo", user.getPhotoUrl().toString());
                    Log.d("Name", user.getDisplayName());
                } else {
                    Toast.makeText(getActivity(), "Authentication failed.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onSuccess(LoginResult loginResult) {

    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onError(FacebookException error) {

    }

    @Override
    public void onCompleted(JSONObject jsonObject, GraphResponse response) {
        try {
            if (jsonObject.has("birthday"))
                Log.e("Fecha Nacimiento", jsonObject.getString("birthday"));
            if (jsonObject.has("email"))
                Log.e("Correo", jsonObject.getString("email"));
        } catch (NullPointerException | JSONException e) {
            e.printStackTrace();
        }
    }
}
