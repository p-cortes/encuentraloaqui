package com.ivancortes.todoen1.presenter.implementation;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.ivancortes.todoen1.App;
import com.ivancortes.todoen1.presenter.callback.BaseCallback;

import javax.inject.Inject;

import dagger.Module;
import io.realm.Realm;
import retrofit2.Retrofit;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by christian.vargas on 1/29/18
 */
public class BasePresenter {

    @Inject
    Retrofit retrofit;
    @Inject
    Realm mRealm;

    private Subscription mSubscription;
    private BaseCallback baseCallback;
    public Observable<Object> generalObservable;
    protected final Context context;

    public BasePresenter(Context context) {
        this.context = context;
    }

    public void onCreate() {
        ((App) context.getApplicationContext()).getAppComponent().inject(this);
        baseCallback = (BaseCallback) this;
        mRealm = Realm.getDefaultInstance();
    }


    public void requestFunction(Observable<Object> classResponse, final int type, final Class cls) {

        mSubscription = classResponse.
                subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        baseCallback.onError(e.getMessage());
                    }

                    @Override
                    public void onNext(Object object) {
                        Gson gson = new Gson();
                        JsonObject jsonObject = gson.toJsonTree(object).getAsJsonObject();
                        Object o = new Gson().fromJson(jsonObject, cls);
                        baseCallback.onSuccess(o, type);
                    }
                });
    }


    public void onStart() {

    }

    public void onResume() {

    }

    public void onPause() {

    }

    public void onStop() {

    }

    public void onDestroy() {
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }
}
