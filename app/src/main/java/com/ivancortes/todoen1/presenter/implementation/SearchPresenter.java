package com.ivancortes.todoen1.presenter.implementation;

import android.content.Context;

import com.ivancortes.todoen1.R;
import com.ivancortes.todoen1.background.web.RestApi;
import com.ivancortes.todoen1.background.ws.responses.CategoriesResponse;
import com.ivancortes.todoen1.models.Announcement;
import com.ivancortes.todoen1.models.AnnouncementsList;
import com.ivancortes.todoen1.models.Category;
import com.ivancortes.todoen1.presenter.callback.BaseCallback;
import com.ivancortes.todoen1.presenter.callback.SearchCallback;
import com.ivancortes.todoen1.utils.Constants;
import com.ivancortes.todoen1.utils.MessageUtils;
import com.ivancortes.todoen1.utils.ServicesKeys;

import java.util.ArrayList;

/**
 * Created by ivancortes on 25/03/18.
 */

public class SearchPresenter extends BasePresenter implements BaseCallback {


    private SearchCallback searchCallback;

    public SearchPresenter(Context context, SearchCallback searchCallback) {
        super(context);
        this.searchCallback = searchCallback;
    }

    public void getCategories() {
        //String auth = Constants.getWSCredentials();
        generalObservable = retrofit.create(RestApi.class).getCategories(Constants.TOKEN, "false");
        requestFunction(generalObservable, ServicesKeys.GET_CATEGORIES, CategoriesResponse.class);
    }

    public void getAnnouncement() {
        generalObservable = retrofit.create(RestApi.class).getAnnouncement(Constants.TOKEN);
        requestFunction(generalObservable, ServicesKeys.GET_ANNOUNCEMENT, AnnouncementsList.class);
    }

    @Override
    public void onSuccess(Object object, int type) {
        switch (type) {
            case ServicesKeys.GET_CATEGORIES:
                CategoriesResponse categoriesResponse = (CategoriesResponse) object;
                categoriesResponse.getCategory().add(new Category("1", "Más", "Categoría para ver listado completo", "", true));
                searchCallback.onSuccessGettingCategories(categoriesResponse);
                break;
            case ServicesKeys.GET_ANNOUNCEMENT:
                searchCallback.onSuccessGetAnnouncement((AnnouncementsList) object);
                break;
            default:
                onError(Constants.WRONG_RESPONSE);
                break;
        }
    }

    @Override
    public void onError(String message) {
        searchCallback.onErrorGettingCategories("Message");
    }


    public ArrayList<Category> generateHolderAdapter() {
        ArrayList<Category> categoryArrayList = new ArrayList<>();
        for (int i = 0; i < 6; i++){
            categoryArrayList.add(new Category(true));
        }
        return categoryArrayList;
    }

}
