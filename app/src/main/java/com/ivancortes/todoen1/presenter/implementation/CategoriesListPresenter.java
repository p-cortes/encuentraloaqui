package com.ivancortes.todoen1.presenter.implementation;

import android.content.Context;

import com.ivancortes.todoen1.R;
import com.ivancortes.todoen1.background.web.RestApi;
import com.ivancortes.todoen1.background.ws.responses.CategoriesResponse;
import com.ivancortes.todoen1.models.Category;
import com.ivancortes.todoen1.presenter.callback.BaseCallback;
import com.ivancortes.todoen1.presenter.callback.CategoriesListCallback;
import com.ivancortes.todoen1.utils.Constants;
import com.ivancortes.todoen1.utils.MessageUtils;
import com.ivancortes.todoen1.utils.ServicesKeys;

/**
 * Created by christian.vargas on 3/25/18.
 */

public class CategoriesListPresenter extends BasePresenter implements BaseCallback{

    private CategoriesListCallback categoriesListCallback;

    public CategoriesListPresenter(Context context, CategoriesListCallback categoriesListCallback) {
        super(context);
        this.categoriesListCallback = categoriesListCallback;
    }



    @Override
    public void onSuccess(Object object, int type) {
        switch (type) {
            case ServicesKeys.GET_CATEGORIES:
                CategoriesResponse categoriesResponse = (CategoriesResponse) object;
                categoriesListCallback.onSuccessGetCategories(categoriesResponse.getCategory());
                break;
            default:
                onError(Constants.WRONG_RESPONSE);
                break;
        }
    }

    @Override
    public void onError(String message) {
        categoriesListCallback.onErrorGetCategories("Message");
    }

    public void getCategories() {
        generalObservable = retrofit.create(RestApi.class).getCategories(Constants.TOKEN, "true");
        requestFunction(generalObservable, ServicesKeys.GET_CATEGORIES, CategoriesResponse.class);
    }

}
