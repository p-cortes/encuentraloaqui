package com.ivancortes.todoen1.presenter.callback;

import com.ivancortes.todoen1.models.ProvidersResponse;

/**
 * Created by christian.vargas on 3/26/18.
 */

public interface ProvidersListCallback {
    void onSuccessGetProviders(ProvidersResponse providersResponse);

    void onErrorGetProviders(String message);
}
