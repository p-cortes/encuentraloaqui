package com.ivancortes.todoen1.presenter.callback;

import com.ivancortes.todoen1.models.Category;

import java.util.ArrayList;

/**
 * Created by christian.vargas on 3/25/18.
 */

public interface CategoriesListCallback {
    void onSuccessGetCategories(ArrayList<Category> category);

    void onErrorGetCategories(String message);
}
