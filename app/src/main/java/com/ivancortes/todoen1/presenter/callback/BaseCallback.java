package com.ivancortes.todoen1.presenter.callback;

/**
 * Created by christian.vargas on 1/29/18
 *
 */

public interface BaseCallback {
    void onSuccess(Object object, int type);
    void onError(String message);
}
