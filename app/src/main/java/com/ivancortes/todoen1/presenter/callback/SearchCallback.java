package com.ivancortes.todoen1.presenter.callback;


import com.ivancortes.todoen1.background.ws.responses.CategoriesResponse;
import com.ivancortes.todoen1.models.AnnouncementsList;

/**
 * Created by ivancortes on 25/03/18.
 */

public interface SearchCallback {
    void onSuccessGettingCategories(CategoriesResponse categoriesResponse);

    void onErrorGettingCategories(String message);

    void onSuccessGetAnnouncement(AnnouncementsList announcementsList);
}
