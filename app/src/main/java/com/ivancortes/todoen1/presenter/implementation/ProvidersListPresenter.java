package com.ivancortes.todoen1.presenter.implementation;

import android.content.Context;

import com.ivancortes.todoen1.background.web.RestApi;
import com.ivancortes.todoen1.background.ws.responses.CategoriesResponse;
import com.ivancortes.todoen1.models.ProvidersResponse;
import com.ivancortes.todoen1.presenter.callback.BaseCallback;
import com.ivancortes.todoen1.presenter.callback.ProvidersListCallback;
import com.ivancortes.todoen1.utils.Constants;
import com.ivancortes.todoen1.utils.ServicesKeys;

/**
 * Created by christian.vargas on 3/26/18.
 */

public class ProvidersListPresenter extends BasePresenter implements BaseCallback {


    private ProvidersListCallback providersListCallback;

    public ProvidersListPresenter(Context context, ProvidersListCallback providersListCallback) {
        super(context);
        this.providersListCallback = providersListCallback;
    }

    @Override
    public void onSuccess(Object object, int type) {
        switch (type) {
            case ServicesKeys.GET_PROVIDERS:
                providersListCallback.onSuccessGetProviders((ProvidersResponse) object);
                break;
            default:
                onError(Constants.WRONG_RESPONSE);
                providersListCallback.onErrorGetProviders("message");
                break;
        }
    }

    @Override
    public void onError(String message) {
        providersListCallback.onErrorGetProviders("message");

    }

    public void getProviders(String categoryId) {
        generalObservable = retrofit.create(RestApi.class).getProviders(Constants.TOKEN, categoryId);
        requestFunction(generalObservable, ServicesKeys.GET_PROVIDERS, ProvidersResponse.class);
    }
}
