package com.ivancortes.todoen1.background.web;

import android.content.Context;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by christian.vargas on 1/29/18.
 *
 */

@Module
public class ModuleNet {

    private String mBaseUrl;
    private OkHttpClient okHttpClient;
    private HttpLoggingInterceptor httpLoggingInterceptor;

    public ModuleNet(String mBaseUrl, Context context){
        this.mBaseUrl = mBaseUrl;
        httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        okHttpClient = new OkHttpClient.Builder()
                .readTimeout(180, TimeUnit.SECONDS)
                .addInterceptor(httpLoggingInterceptor)
                .connectTimeout(180, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    Retrofit retrofitProvider(){
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(mBaseUrl)
                .client(okHttpClient)
                .build();
    }
}

