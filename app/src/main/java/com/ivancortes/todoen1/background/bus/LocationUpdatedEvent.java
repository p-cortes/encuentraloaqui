package com.ivancortes.todoen1.background.bus;

import android.location.Location;

/**
 * Created by Mora on 20/02/2018
 */

public class LocationUpdatedEvent {
    private Location location;

    public LocationUpdatedEvent(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
