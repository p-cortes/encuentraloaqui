package com.ivancortes.todoen1.background.web;

import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by christian.vargas on 1/29/18.
 *
 */

public interface RestApi {
    @GET("business/api/v1/categories/")
    Observable<Object> getCategories(@Header("Authorization") String token, @Query("all") String getAll);


    @GET("business/api/v1/announcement")
    Observable<Object> getAnnouncement(@Header("Authorization") String token);

    @GET("business/api/v1/providers/")
    Observable<Object> getProviders(@Header("Authorization") String token, @Query("category") String categoryId);
}
