package com.ivancortes.todoen1.background.ws.responses;

import com.google.gson.annotations.SerializedName;
import com.ivancortes.todoen1.models.Category;

import java.util.ArrayList;

/**
 * Created by Mora on 24/03/2018
 *
 */

public class CategoriesResponse {


    @SerializedName("results")
    private ArrayList<Category> category;

    public ArrayList<Category> getCategory() {
        return category;
    }

    public void setCategory(ArrayList<Category> category) {
        this.category = category;
    }
}
