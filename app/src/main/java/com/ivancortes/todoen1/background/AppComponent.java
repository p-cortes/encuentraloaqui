package com.ivancortes.todoen1.background;

import com.ivancortes.todoen1.background.data.ModuleRealm;
import com.ivancortes.todoen1.background.web.ModuleNet;
import com.ivancortes.todoen1.presenter.implementation.BasePresenter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by ivancortes on 24/03/18.
 */

@Singleton
@Component(modules = {ModuleNet.class, ModuleRealm.class})

public interface AppComponent {

    void inject(BasePresenter basePresenter);

}