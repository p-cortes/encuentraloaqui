package com.ivancortes.todoen1.background.data;

import android.content.Context;

import java.security.SecureRandom;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by christian.vargas on 1/29/18.
 *
 */

@Module
public class ModuleRealm {

    private Context context;

    public ModuleRealm(Context context){
        this.context = context;
    }

    @Provides
    @Singleton
    Realm realmProvider() {
        byte[] key = new byte[64];
        new SecureRandom(key);
        Realm.init(context);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .encryptionKey(key).build();

        Realm.setDefaultConfiguration(realmConfiguration);
        return Realm.getDefaultInstance();
    }
}

