package com.ivancortes.todoen1.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.ivancortes.todoen1.R;

/**
 * Created by dflabs on 11/22/16.
 * EMP
 */
public class MessageUtils {
    private static Toast toast = null;
    private static ProgressDialog sProgressDialog;
    public static void toast(Context context, int message) {
        if (context == null) return;
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        toast.show();
    }
    public static void toast(Context context, String message) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        toast.show();
    }
    public static void progress(Context context, int message) {
        stopProgress();
        sProgressDialog = ProgressDialog.show(context, null, context.getString(message), true, false);
        sProgressDialog.show();
    }
    public static void stopProgress() {
        try {
            if (sProgressDialog != null) {
                sProgressDialog.cancel();
                sProgressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
/*    public static void saveExitConfirm(AppCompatActivity appCompatActivity,
                                       int message,
                                       DialogInterface.OnClickListener clickBackListener,
                                       DialogInterface.OnClickListener clickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(appCompatActivity)
                .setMessage(message)
                .setNeutralButton(R.string.dialog_save, clickListener)
                .setPositiveButton(R.string.dialog_accept, clickBackListener)
                .setNegativeButton(R.string.dialog_cancel, null);
        builder.create().show();
    }*/
    public static void confirm(Context context, int message, DialogInterface.OnClickListener clickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(R.string.dialog_accept, clickListener)
                .setNegativeButton(R.string.dialog_cancel, null);
        builder.create().show();
    }
    public static void retry(Context context, int message, DialogInterface.OnClickListener clickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton(R.string.dialog_retry, clickListener);
        builder.create().show();
    }
}