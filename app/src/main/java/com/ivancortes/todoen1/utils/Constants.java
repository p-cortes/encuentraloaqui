package com.ivancortes.todoen1.utils;

import android.util.Base64;

/**
 * Created by Mora on 24/03/2018
 *
 */

public class Constants {
    public static final String WRONG_RESPONSE = "Successful call with incorrect response type";
    private static final String WS_USER = "WSUser";
    private static final String WS_WORD = "Passw0rd0";
    public static final String CATEGORY_ID = "provider_id";
    public static final String TOKEN = "token f8e77ef85a4a618ac4ebb92b88ec747c5445d461";

    public static String getWSCredentials() {
        String credentials = Constants.WS_USER + ":" + Constants.WS_WORD;
        String encoding = Base64.encodeToString((credentials).getBytes(), Base64.NO_WRAP);
        return "Basic " + encoding;
    }
}
