package com.ivancortes.todoen1.utils;

/**
 * Created by Mora on 24/03/2018
 *
 */

public enum ServicesKeys {
    ;
    public static final int GET_CATEGORIES = 1;
    public static final int GET_ANNOUNCEMENT = 2;
    public static final int GET_PROVIDERS = 3;
}
