package com.ivancortes.todoen1;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.ivancortes.todoen1.background.AppComponent;
import com.ivancortes.todoen1.background.DaggerAppComponent;
import com.ivancortes.todoen1.background.data.ModuleRealm;
import com.ivancortes.todoen1.background.web.ModuleNet;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;

/**
 * Created by christian.vargas on 1/29/18.
 *
 */

public class App extends Application {
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder()
                .moduleNet(new ModuleNet("http://188.166.105.203:8000/", getApplicationContext()))
                .moduleRealm(new ModuleRealm(getApplicationContext()))
                .build();

        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(this.getResources().getString(R.string.CONSUMER_KEY), this.getResources().getString(R.string.CONSUMER_SECRET)))
                .debug(true)
                .build();
        Twitter.initialize(config);
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }


}
